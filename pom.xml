<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.gaptap.bamboo</groupId>
	<version>0.4.0-SNAPSHOT</version>
	<organization>
		<name>David Ehringer</name>
		<url>https://gaptap.atlassian.net</url>
	</organization>
	<name>Bamboo Auditor Plugin</name>
	<description>An Atlassian Bamboo plugin providing build plan and deployment project auditing.</description>
	<packaging>atlassian-plugin</packaging>
	<inceptionYear>2014</inceptionYear>
	<developers>
		<developer>
			<name>David Ehringer</name>
			<organizationUrl>http://davidehringer.com</organizationUrl>
		</developer>
	</developers>
	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0</url>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:https://bitbucket.org/dehringer/bamboo-auditor-plugin.git</connection>
		<developerConnection>scm:git:https://bitbucket.org/dehringer/bamboo-auditor-plugin.git</developerConnection>
		<url>https://bitbucket.org/dehringer/bamboo-auditor-plugin/src</url>
	</scm>

	<properties>
		<bamboo.version>5.15.7</bamboo.version>
		<bamboo.data.version>5.15.7</bamboo.data.version>
		<amps.version>6.2.11</amps.version>
		<ao.version>1.1.5</ao.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-web</artifactId>
			<version>${bamboo.version}</version>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<!-- Not available in public repos -->
					<groupId>javax.jms</groupId>
					<artifactId>jms</artifactId>
				</exclusion>
				<exclusion>
					<artifactId>commons-logging</artifactId>
					<groupId>commons-logging</groupId>
				</exclusion>
				<exclusion>
					<artifactId>hamcrest-all</artifactId>
					<groupId>org.hamcrest</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>com.atlassian.activeobjects</groupId>
			<artifactId>activeobjects-plugin</artifactId>
			<version>${ao.version}</version>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<artifactId>commons-logging</artifactId>
					<groupId>commons-logging</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo.plugins</groupId>
			<artifactId>atlassian-bamboo-plugin-git</artifactId>
			<version>${bamboo.version}</version>
			<type>atlassian-plugin</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo.plugins</groupId>
			<artifactId>atlassian-bamboo-plugin-mercurial</artifactId>
			<version>${bamboo.version}</version>
			<type>atlassian-plugin</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo.plugins</groupId>
			<artifactId>atlassian-bamboo-plugin-bitbucket</artifactId>
			<version>${bamboo.version}</version>
			<type>atlassian-plugin</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo.plugins.stash</groupId>
			<artifactId>atlassian-bamboo-plugin-stash</artifactId>
			<version>${bamboo.version}</version>
			<type>atlassian-plugin</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.10</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.scala-lang</groupId>
			<artifactId>scala-library</artifactId>
			<version>2.10.4</version>
		</dependency>
		<dependency>
			<groupId>com.atlassian.sal</groupId>
			<artifactId>sal-api</artifactId>
			<version>3.0.2</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.google.collections</groupId>
			<artifactId>google-collections</artifactId>
			<version>1.0</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>com.atlassian.maven.plugins</groupId>
				<artifactId>maven-bamboo-plugin</artifactId>
				<version>${amps.version}</version>
				<extensions>true</extensions>
				<configuration>
					<productVersion>${bamboo.version}</productVersion>
					<productDataVersion>${bamboo.data.version}</productDataVersion>
					<productDataPath>${project.basedir}/src/test/resources/generated-test-resources.zip</productDataPath>
					<enableQuickReload>true</enableQuickReload>
					<enableFastdev>false</enableFastdev>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.5.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>com.atlassian.maven.plugins</groupId>
										<artifactId>maven-bamboo-plugin</artifactId>
										<versionRange>[3.11,)</versionRange>
										<goals>
											<goal>compress-resources</goal>
											<goal>generate-rest-docs</goal>
											<goal>generate-manifest</goal>
											<goal>filter-plugin-descriptor</goal>
											<goal>copy-test-bundled-dependencies</goal>
											<goal>filter-test-plugin-descriptor</goal>
											<goal>copy-bundled-dependencies</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore />
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<repositories>
		<repository>
			<id>atlassian-public</id>
			<url>https://m2proxy.atlassian.com/repository/public</url>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
				<checksumPolicy>warn</checksumPolicy>
			</snapshots>
			<releases>
				<enabled>true</enabled>
				<checksumPolicy>warn</checksumPolicy>
			</releases>
		</repository>
	</repositories>
	<pluginRepositories>
		<pluginRepository>
			<id>atlassian-public</id>
			<url>https://m2proxy.atlassian.com/repository/public</url>
			<releases>
				<enabled>true</enabled>
				<checksumPolicy>warn</checksumPolicy>
			</releases>
			<snapshots>
				<updatePolicy>never</updatePolicy>
				<checksumPolicy>warn</checksumPolicy>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>
	<artifactId>bamboo-auditor-plugin</artifactId>
</project>

# Bamboo Auditor Plugin #

An Atlassian Bamboo plugin providing build plan and deployment project auditing.

Currently in experimentation phase...

Please submit enhancement requests or issues at https://gaptap.atlassian.net/browse/AUDIT.
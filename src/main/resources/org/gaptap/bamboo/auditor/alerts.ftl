[#import "/lib/build.ftl" as bd]

<html>
<head>
	[@ui.header pageKey="auditor.admin.alerts.title" title=true /]
	<meta name="decorator" content="adminpage">
    ${webResourceManager.requireResource("org.gaptap.bamboo.auditor.auditor-plugin:auditor-plugin-resources")}
</head>
<body>
	[@ui.header pageKey="auditor.admin.alerts.heading" /]
	<p>
		<img src="${req.contextPath}/download/resources/org.gaptap.bamboo.bamboo-auditor-plugin:auditor-plugin-resources/images/icon_13482.png" style="float: left; margin-right: 5px" width="32" height="32" />
	</p>
	[@ww.actionmessage /]
	[@ui.clear/]
	
	[@ui.messageBox type='warning' titleKey='Experimental']
		Alerts are experimental and may be changed or removed at any time.
	[/@ui.messageBox]
	
	<table id="reports" class="aui" width="100%">
		<thead><tr>
			<th class="labelPrefixCell">Action</th>
			<th class="valueCell">Report</th>
			<th class="valueCell">Description</th>
		</tr></thead>
		<tbody>	
			<tr>
				<td class="labelPrefixCell">
					<div class="toolbar">
						<div class="aui-toolbar inline">
							<ul class="toolbar-group">
								<li class="toolbar-item">
									<a class="toolbar-trigger" href="[@ww.url action='enableExampleAlert' namespace='/admin/auditor' /]">[@ww.text name='auditor.admin.alerts.enable' /]</a>
								</li>
							</ul>
						</div>
					</div>
				</td>
				<td class="valueCell">Example Alert</td>
				<td class="valueCell"></td>
			</tr>
		</tbody>
	</table>
	[@ww.form action=updateUrl
		cancelUri=cancelUrl
		submitLabelKey='global.buttons.update'
		id='updateSystemNotificationForm'
		showActionErrors='false'
		]
    	[@commonNotificationFormContent /]
	[/@ww.form]
</body>
</html>

[#macro commonNotificationFormContent showActionErrors=true ]
    [#if showActionErrors]
        [@ww.actionerror /]
    [/#if]

    [@ww.select labelKey='notification.recipients.types' name='notificationRecipientType'
    list=allNotificationRecipientTypes listKey='key' listValue='description' toggle='true'/]

    [#list allNotificationRecipientTypes as recipient]
        [@ui.bambooSection dependsOn='notificationRecipientType' showOn='${recipient.key}']
        ${recipient.getEditHtml()}
        [/@ui.bambooSection]
    [/#list]
[/#macro]

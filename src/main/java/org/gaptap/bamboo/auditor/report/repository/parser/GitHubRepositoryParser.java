/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.repository.parser;

import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.auditor.report.repository.RepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.RepositoryTableRow;
import static com.atlassian.bamboo.utils.ConfigUtils.getXmlConfigFromXmlString;


/**
 * @author David Ehringer
 */
public class GitHubRepositoryParser implements RepositoryParser {
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(GitHubRepositoryParser.class);

    @Override
    public boolean canParse(PlanRepositoryDefinition planRepositoryDefinition) {
        String pluginKey = planRepositoryDefinition.getPluginKey();

        return "com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:gh".equals(pluginKey);
    }

    @Override
    public void parse(PlanRepositoryDefinition planRepositoryDefinition, RepositoryTableRow row) {
        LOG.debug(planRepositoryDefinition.getVcsLocation().getLegacyConfigurationXml());
        String serverConfigXML = planRepositoryDefinition.getVcsLocation().getLegacyConfigurationXml();

        final XMLConfiguration legacyConfig = getXmlConfigFromXmlString(serverConfigXML);
        String repoUser = legacyConfig.getString("repository.github.username");
        String repoPass = legacyConfig.getString("repository.github.password");
        String repoUrl = legacyConfig.getString("repository.github.repository");

        row.setRepositoryType("GitHub");
        row.setRepositoryUrl(repoUrl);
        if (StringUtils.isEmpty(repoPass)) {
            row.setAuthType("anonymous");
        } else {
            row.setAuthType("password");
            row.setCredentials(repoUser);
        }

    }

}

/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.permissions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.acegisecurity.acls.AccessControlEntry;
import org.acegisecurity.acls.MutableAcl;
import org.acegisecurity.acls.Permission;
import org.acegisecurity.acls.sid.GrantedAuthoritySid;
import org.acegisecurity.acls.sid.PrincipalSid;
import org.acegisecurity.acls.sid.Sid;
import org.gaptap.bamboo.auditor.report.AbstractTableReport;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.InternalEnvironment;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.security.acegi.acls.GroupPrincipalSid;
import com.atlassian.bamboo.security.acegi.acls.HibernateMutableAclService;
import com.atlassian.bamboo.security.acegi.acls.HibernateObjectIdentityImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author David Ehringer
 */
public class PermissionsReport extends AbstractTableReport<PermissionsReportLine> {

    private final HibernateMutableAclService mutableAclService;
    private final PlanManager planManager;
    private final DeploymentProjectService deploymentProjectService;
    private final AdministrationConfiguration administrationConfiguration;

    public PermissionsReport(HibernateMutableAclService mutableAclService, PlanManager planManager,
            DeploymentProjectService deploymentProjectService, AdministrationConfiguration administrationConfiguration) {
        this.mutableAclService = mutableAclService;
        this.planManager = planManager;
        this.deploymentProjectService = deploymentProjectService;
        this.administrationConfiguration = administrationConfiguration;
    }

    @Override
    public List<String> getColumnsNames() {
        return PermissionsReportLine.getColumsNames();
    }

    public void generate() {
        for (TopLevelPlan plan : planManager.getAllPlans(TopLevelPlan.class)) {
            analyze(plan);
            for (DeploymentProject project : deploymentProjectService.getDeploymentProjectsRelatedToPlan(plan
                    .getPlanKey())) {
                analyze(plan, project);
                for (Environment environment : project.getEnvironments()) {
                    analyze(project, environment);
                }
            }
        }
    }

    private void analyze(TopLevelPlan plan) {
        MutableAcl acl = mutableAclService.readMutableAclById(new HibernateObjectIdentityImpl(plan));
        addRows(generateLines(plan.getBuildName(), "Build", plan.getProject().getName(), acl, buildLink(plan)));
    }

    private void analyze(TopLevelPlan plan, DeploymentProject project) {
        MutableAcl acl = mutableAclService.readMutableAclById(project.getId());
        addRows(generateLines(project.getName(), "Deployment Project", plan.getName(), acl, buildLink(project)));
    }

    private void analyze(DeploymentProject project, Environment environment) {
        MutableAcl acl = mutableAclService.readMutableAclById(new HibernateObjectIdentityImpl(
                InternalEnvironment.class, environment.getId()));
        addRows(generateLines(environment.getName(), "Environment", project.getName(), acl, buildLink(environment)));
    }

    private String buildLink(TopLevelPlan plan) {
        return administrationConfiguration.getBaseUrl() + "/chain/admin/config/editChainPermissions.action?buildKey="
                + plan.getPlanKey().getKey();
    }

    private String buildLink(DeploymentProject project) {
        return administrationConfiguration.getBaseUrl()
                + "/deploy/config/configureDeploymentProjectPermissions.action?deploymentProjectId=" + project.getId();
    }

    private String buildLink(Environment environment) {
        return administrationConfiguration.getBaseUrl()
                + "/deploy/config/configureEnvironmentPermissions.action?environmentId=" + environment.getId();
    }

    private List<PermissionsReportLine> generateLines(String entityName, String type, String parent, MutableAcl acl,
            String link) {
        List<PermissionsReportLine> lines = Lists.newArrayList();

        Map<Principal, List<Permission>> permissions = Maps.newHashMap();
        for (AccessControlEntry entry : acl.getEntries()) {
            Principal principal = toPrincipal(entry);
            if (!permissions.containsKey(principal)) {
                permissions.put(principal, new ArrayList<Permission>());
            }
            permissions.get(principal).add(entry.getPermission());
        }
        for (Principal principal : permissions.keySet()) {
            PermissionsReportLine line = new PermissionsReportLine();
            lines.add(line);

            line.setEntity(entityName);
            line.setType(type);
            line.setParent(parent);
            line.setPrincipal(principal);
            line.setLink(link);
            for (Permission permission : permissions.get(principal)) {
                line.set(permission);
            }
        }
        return lines;
    }

    private Principal toPrincipal(AccessControlEntry from) {
        Sid sid = from.getSid();
        if (sid instanceof GroupPrincipalSid) {
            GroupPrincipalSid gsid = (GroupPrincipalSid) sid;
            return new Principal(gsid.getPrincipal(), "GROUP");
        }
        if (sid instanceof GrantedAuthoritySid) {
            GrantedAuthoritySid gsid = (GrantedAuthoritySid) sid;
            return new Principal(gsid.getGrantedAuthority(), "GRANTED_AUTHORITY");
        }
        if (sid instanceof PrincipalSid) {
            PrincipalSid gsid = (PrincipalSid) sid;
            return new Principal(gsid.getPrincipal(), "USER");
        }
        return new Principal(sid.toString(), "UNKNOWN");
    }
}

/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.repository.parser;

import org.gaptap.bamboo.auditor.report.repository.RepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.RepositoryTableRow;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;

import java.util.Map;

/**
 * @author Jeanyhwh Desulme
 */
public class StashRepositoryParser implements RepositoryParser {
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(StashRepositoryParser.class);

    @Override
    public boolean canParse(PlanRepositoryDefinition planRepositoryDefinition) {
        String pluginKey = planRepositoryDefinition.getPluginKey();

        return "com.atlassian.bamboo.plugins.stash.atlassian-bamboo-plugin-stash:bbserver".equals(pluginKey);
    }

    @Override
    public void parse(PlanRepositoryDefinition planRepositoryDefinition, RepositoryTableRow row) {
        LOG.debug(planRepositoryDefinition.getVcsLocation().getConfiguration());

        Map<String, String> serverConfig = planRepositoryDefinition.getVcsLocation().getConfiguration();

        String repoUrl = serverConfig.get("repository.stash.repositoryUrl");

        row.setRepositoryType("Bitbucket Server");
        row.setRepositoryUrl(repoUrl);
        row.setAuthType("ssh_keypair");
    }
}
/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.permissions;

import java.util.List;

import org.acegisecurity.acls.Permission;
import org.gaptap.bamboo.auditor.report.TableRow;

import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
public class PermissionsReportLine implements TableRow {

    private static final List<String> COLUMN_HEADINGS = Lists.newArrayList("Entity Name", "Type", "Parent", "Principal",
            "Principal Type", "READ", "WRITE", "BUILD/DEPLOY", "CLONE", "ADMINISTRATION", "Link");

//    private static final String SEPARATOR = ",";

    private String type;
    private String parent;
    private String entity;
    private Principal principal;
    private boolean read = false;
    private boolean write = false;
    private boolean build = false;
    private boolean clone = false;
    private boolean admin = false;
    private String link;

    public void set(Permission permission) {
        if (BambooPermission.ADMINISTRATION.equals(permission)) {
            admin = true;
        } else if (BambooPermission.READ.equals(permission)) {
            read = true;
        } else if (BambooPermission.WRITE.equals(permission)) {
            write = true;
        } else if (BambooPermission.BUILD.equals(permission)) {
            build = true;
        } else if (BambooPermission.CLONE.equals(permission)) {
            clone = true;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

    public boolean isRead() {
        return read;
    }

    public boolean isWrite() {
        return write;
    }

    public boolean isBuild() {
        return build;
    }

    public boolean isClone() {
        return clone;
    }

    public boolean isAdmin() {
        return admin;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

//    public String toCsv() {
//        StringBuilder csv = new StringBuilder();
//        csv.append(StringEscapeUtils.escapeCsv(entity));
//        csv.append(SEPARATOR);
//        csv.append(StringEscapeUtils.escapeCsv(type));
//        csv.append(SEPARATOR);
//        csv.append(StringEscapeUtils.escapeCsv(parent));
//        csv.append(SEPARATOR);
//        csv.append(StringEscapeUtils.escapeCsv(principal.getName()));
//        csv.append(SEPARATOR);
//        csv.append(StringEscapeUtils.escapeCsv(principal.getType()));
//        csv.append(SEPARATOR);
//        csv.append(isEnabled(read));
//        csv.append(SEPARATOR);
//        csv.append(isEnabled(write));
//        csv.append(SEPARATOR);
//        csv.append(isEnabled(build));
//        csv.append(SEPARATOR);
//        csv.append(isEnabled(clone));
//        csv.append(SEPARATOR);
//        csv.append(isEnabled(admin));
//        csv.append(SEPARATOR);
//        csv.append(StringEscapeUtils.escapeCsv(link));
//        csv.append(SEPARATOR);
//        return csv.toString();
//    }

    private String isEnabled(boolean flag) {
        return flag == true ? "x" : "";
    }

    public static List<String> getColumsNames() {
        return COLUMN_HEADINGS;
    }

    @Override
    public List<String> getData() {
        return Lists.newArrayList(entity, type, parent, principal.getName(), principal.getType(), isEnabled(read),
                isEnabled(write), isEnabled(build), isEnabled(clone), isEnabled(admin), link);
    }
}

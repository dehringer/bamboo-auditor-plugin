/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.repository.parser;

import com.atlassian.bamboo.repository.svn.v2.configurator.SvnServerConfigurator;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.vcs.configuration.VcsLocationDefinition;
import com.atlassian.bamboo.vcs.configuration.VcsRepositoryData;
import org.gaptap.bamboo.auditor.report.repository.RepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.RepositoryTableRow;

import com.atlassian.bamboo.repository.AuthenticationType;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.svn.SvnRepository;

import java.util.Map;

/**
 * @author David Ehringer
 */
public class SvnRepositoryParser implements RepositoryParser {
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(SvnRepositoryParser.class);

    @Override
    public boolean canParse(PlanRepositoryDefinition planRepositoryDefinition) {
        String pluginKey = planRepositoryDefinition.getPluginKey();

        return "com.atlassian.bamboo.plugin.system.repository:svnv2".equals(pluginKey);
    }


    @Override
    public void parse(PlanRepositoryDefinition planRepositoryDefinition, RepositoryTableRow row) {
        LOG.debug(planRepositoryDefinition.getVcsLocation().getConfiguration());

        Map<String, String> serverConfig = planRepositoryDefinition.getVcsLocation().getConfiguration();

        String repoUrl = serverConfig.get("repository.svn.repositoryRoot");
        String repoUser = serverConfig.get("repository.svn.username");
        String authType = serverConfig.get("repository.svn.authType");
        String keyFile = serverConfig.get("repository.svn.keyFile");


        row.setRepositoryType("SVN");
        row.setRepositoryUrl(repoUrl);
        row.setAuthType(authType);

        if (AuthenticationType.PASSWORD.getKey().equals(authType)) {
            row.setCredentials(repoUser);
        } else if (AuthenticationType.SSH.getKey().equals(authType)) {
            row.setCredentials(keyFile);
        } else if (AuthenticationType.SSL_CLIENT_CERTIFICATE.getKey().equals(authType)) {
            row.setCredentials(keyFile);
        }

    }

}

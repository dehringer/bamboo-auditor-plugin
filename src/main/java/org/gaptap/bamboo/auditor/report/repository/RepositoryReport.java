/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.auditor.report.repository;

import java.util.List;

import com.atlassian.bamboo.vcs.configuration.VcsLocationDefinition;
import org.gaptap.bamboo.auditor.report.AbstractTableReport;
import org.gaptap.bamboo.auditor.report.repository.parser.BitBucketRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.CvsRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.GitHubRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.GitRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.MercurialRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.PerforceRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.SvnRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.StashRepositoryParser;
import org.gaptap.bamboo.auditor.report.repository.parser.UnknownRepositoryParser;

import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.vcs.configurator.VcsLocationConfigurator;
import com.atlassian.bamboo.vcs.viewer.configuration.VcsRepositoryViewerDefinition;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.RepositoryDefinition;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
public class RepositoryReport extends AbstractTableReport<RepositoryTableRow> {
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(RepositoryReport.class);

    private static final List<? extends RepositoryParser> repoParsers = Lists.newArrayList(new SvnRepositoryParser(),
            new GitHubRepositoryParser(), new GitRepositoryParser(), new MercurialRepositoryParser(),
            new BitBucketRepositoryParser(), new PerforceRepositoryParser(), new CvsRepositoryParser(),
            new StashRepositoryParser(), new UnknownRepositoryParser());

    private final PlanManager planManager;
    private final AdministrationConfiguration administrationConfiguration;

    public RepositoryReport(PlanManager planManager, AdministrationConfiguration administrationConfiguration) {
        this.planManager = planManager;
        this.administrationConfiguration = administrationConfiguration;
    }

    @Override
    public List<String> getColumnsNames() {
        return RepositoryTableRow.COLUMN_HEADINGS;
    }

    public void generate() {
        for (TopLevelPlan plan : planManager.getAllPlans(TopLevelPlan.class)) {
            for (PlanRepositoryDefinition planRepositoryDefinition : plan.getPlanRepositoryDefinitions()) {
                RepositoryTableRow row = new RepositoryTableRow(administrationConfiguration.getBaseUrl());
                addRow(row);

                String repoKey = planRepositoryDefinition.getPluginKey();

                LOG.info(repoKey);

                row.setPlan(plan.getName());
                row.setPlanKey(plan.getPlanKey().getKey());
                row.setProject(plan.getProject().getName());
                row.setProjectKey(plan.getProject().getKey());
                row.setRepositoryType(repoKey);

                for (RepositoryParser parser : repoParsers) {
                    if (parser.canParse(planRepositoryDefinition)) {
                        parser.parse(planRepositoryDefinition, row);
                        break;
                    }
                }
            }
        }
    }
}
